SuiteCharts: Real Time NetSuite Business Intelligence
===============================================================

SuiteCharts is a enterprise level mobile business intelligence app. The goal is to allow executives and upper management individuals to make intelligent business decisions from anywhere.

SuiteChats utilizes the NetSuite SuiteTalk RESTlet functionality to pull data from already existing Saved Searches. Some setup/implementation is required on the clients NetSuite account, namely deploying the javascript RESTlet. 

Data is displayed in three chart types, bar, pie and line chart. Data is cached for quick access.

corporate site: http://suitecharts.com
for more info email: matt@suitecharts.com

Copyright 2014 MG Limited

