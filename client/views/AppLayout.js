Template.AppLayout.events({
	'click .show-add-saved-search-dialog': function () {
		$("#addSavedSearchModal").modal("show");
	}
});

Template.AppLayout.rendered = function () {
	console.warn = function () {};
}
