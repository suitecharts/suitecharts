
Template.Charts_C3.helpers({
  suiteChart: function() {
    function extractColumnsFrom (headings) {
      var columns = [];

      _.each(headings, function (heading) {
        // Make an array for each column in this format:
        // ['caption', 30, 200, 100, 400, 150];
        
        var contents = _.map(currentData.data, function (dataPoint) {
          return _.findIn(dataPoint, heading.description); // change to heading.path
        });
        var column = [heading.description].concat(contents);
        
        columns.push(column);
      });

      return columns;
    }

    var currentData = Template.currentData();

    var availableNumerics = _(currentData.headings).where({type: "number"});
    var availableCaptions = _(currentData.headings).where({type: "string"});

    var dataColumns = extractColumnsFrom(availableNumerics);

    if (!dataColumns.length) {
      $(".chartWrapper").addClass("hide");
      $(".chartOptions").addClass("hide");
      //swal("Wrong data type", "This search contains no numerical data and probably won't render anything meaningful (if at all) into a chart.", "warning");
    }

    // List and populate any captions we have available in the data
    // (these are just headings with string data sets for now)
    var axisCaptions = extractColumnsFrom(availableCaptions);

    // We can only use one caption until we figure out something besser
    var caption = _.last(axisCaptions) || [];

    if (!currentData) swal("Error", "Error trying to load the chart data. Please refresh the page and try again.", "error", function (){
      //Router.go("searchList");
    })

    return {
      data: {
        x: caption[0],
        columns: [caption].concat(dataColumns),
        type: currentData.chartType
      },
      legend: {
        show: false
      },
      axis: {
        x: { type: 'category' }
      }
    };
  }
});


Template.Charts_C3.rendered = function () {
  function extractColumnsFrom (headings) {
      var columns = [];

      _.each(headings, function (heading) {
        // Make an array for each column in this format:
        // ['caption', 30, 200, 100, 400, 150];
        
        var contents = _.map(currentData.data, function (dataPoint) {
          return _.findIn(dataPoint, heading.description); // change to heading.path
        });
        var column = [heading.description].concat(contents);
        
        columns.push(column);
      });

      return columns;
    }

    var currentData = Template.currentData();

    var availableNumerics = _(currentData.headings).where({type: "number"});
    var availableCaptions = _(currentData.headings).where({type: "string"});

    var dataColumns = extractColumnsFrom(availableNumerics);

    if (!dataColumns.length) {
      $(".chartWrapper").addClass("hide");
      $(".chartOptions").addClass("hide");
      //swal("Wrong data type", "This search contains no numerical data and probably won't render anything meaningful (if at all) into a chart.", "warning");
    }

    // List and populate any captions we have available in the data
    // (these are just headings with string data sets for now)
    var axisCaptions = extractColumnsFrom(availableCaptions);

    // We can only use one caption until we figure out something besser
    var caption = _.last(axisCaptions) || [];

    if (!currentData) swal("Error", "Error trying to load the chart data. Please refresh the page and try again.", "error", function (){
      //Router.go("searchList");
    })

    return {
      data: {
        x: caption[0],
        columns: [caption].concat(dataColumns),
        type: currentData.chartType
      },
      legend: {
        show: true
      },
      axis: {
        x: { type: 'indexed' }
      }
    };
}
