Template.Charts_Datatable.helpers({
	searchData: function () {
		console.log(Template.currentData());
		return searchData;
	},
	tableOptions: tableOptions
});

function searchData () {
	var tmplData = Router.current().data() || {};
	return tmplData.data || [];
}

var previousId;

function tableOptions () {
	var tmplData = Router.current().data() || {};
	var options = {
		columns: [],
		// XXX: set up other defaults here
	};
	
	// if the headings had sorting indexes, we would sort the array here ...

	_.each(tmplData.headings, function (heading) {
		options.columns.push({
			title: heading.description,
			data: heading.description, // this should be renamed to heading.path eventually
			// class: heading.colour // XXX: Matt would like colours in the next release, do this here as a CSS class per colour
		});
	});

	if (!tmplData) {
		swal("No data found",
			"Error loading your data. Please try again.");
		return options;
	}

	return options;
}