Template.Charts.rendered = function () {
  var currentData = Template.currentData();
  $('#chartDescription').append(currentData.description);
  
    function getNewData(){
    	var tmplData = Router.current().data();
      	Meteor.call('netsuite/updateCache', tmplData.searchId, tmplData.options, function (err) {});
      	tmplData = null;
      	currentData = null;
    }
  
  dataInterval = Meteor.setInterval(getNewData, 2500);
}

Template.Charts.destroyed = function () {
    Meteor.clearInterval(dataInterval);
}