Template.Header.helpers({
	isAdmin: function () {
		return Roles.userIsInRole(Meteor.userId(), 'admin');
	},
	savedSearches: function () {
		// Ensure the searchIds as strings (not numbers)
		// Important for conversion to regex for isActiveRoute etc.
		return Netsuite.Searches.find().map(function (value) {
			value.searchId = value.searchId + "";
			return value;
		});
	},
});

$(function() {
    var xpathname = window.location.pathname;
    if (xpathname == '/') {
        $('body').addClass('home');
    }
});

Template.Header.events({
	'click .promote-user-to-role': function (e) {
		var newRole = $(e.currentTarget).data("new-role");
		var $modal = $('#promoteUserModal');
		
		$modal.modal('show');
		$modal.find('#role').val(newRole);
		$modal.find('.role-span').text(newRole);
		
		console.log(newRole);
	},
	'click .learnMoreButton': function (e) {
	    e.stopPropagation();
	    $('html, body').animate({
	        scrollTop: $("#features").offset().top - 50
	    }, 420);
	},
	'click .plansButton': function (e) {
	    e.stopPropagation();
	    $('html, body').animate({
	        scrollTop: $("#plans").offset().top - 70
	    }, 420);
	},
	'click #main-nav .dropdown-menu': function (e){
		e.stopPropagation();
	}
});

