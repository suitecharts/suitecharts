Template.Header_Charts_Cog_Dropdown.events({
	'click [data-action=refresh-chart]': function () {
		Netsuite.search(this.searchId, {forceUpdate: true});
	},
	'click [data-chart-type]': function (e) {
	  var searchDocId = this._id;
	  var chartType = $(e.currentTarget).data("chart-type");
	  
	  Netsuite.Searches.update(searchDocId, {
	  	$set: {chartType: chartType}
	  });
	}
});


