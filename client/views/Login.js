Template.Login.events({
    "submit #loginForm": function(e){
        e.preventDefault();
        var email = $('#email').val();
        var password = $('#password').val();
        Meteor.loginWithPassword(email, password, function(err){
            if(err) console.log(err);
        });
    },
    'click #signupSmallBiz': function (e) {
	    e.stopPropagation();
	    $("html, body").animate({ scrollTop: 0 }, 420);
	    if ($(window).width() < 768) {
		   $('.navbar-header button').click();
		   $('#login-dropdown-list .dropdown-toggle').click();
		   $('#signup-link').click();
		}
		else {
		   $("#login-dropdown-list").find('[data-toggle=dropdown]').dropdown('toggle');
		   $('#signup-link').click();	
		}
	    $('#login-plan-1').click();
	},
	'click #signupProPlan': function (e) {
	    e.stopPropagation();
	    $("html, body").animate({ scrollTop: 0 }, 420);
	    if ($(window).width() < 768) {
		   $('.navbar-header button').click();
		   $('#login-dropdown-list .dropdown-toggle').click();
		   $('#signup-link').click();
		}
		else {
		   $("#login-dropdown-list").find('[data-toggle=dropdown]').dropdown('toggle');
		   $('#signup-link').click();	
		}
	    $('#login-plan-2').click();
	},
	'click #signUpEnterprise': function (e) {
	    e.stopPropagation();
	    $("html, body").animate({ scrollTop: 0 }, 420);
	    if ($(window).width() < 768) {
		   $('.navbar-header button').click();
		   $('#login-dropdown-list .dropdown-toggle').click();
		   $('#signup-link').click();
		}
		else {
		   $("#login-dropdown-list").find('[data-toggle=dropdown]').dropdown('toggle');
		   $('#signup-link').click();	
		}
	    $('#login-plan-3').click();
	},
	'click .learnMoreButton': function (e) {
	    e.stopPropagation();
	    $('html, body').animate({
	        scrollTop: $("#features").offset().top - 50
	    }, 420);
	},
	'click .plansButton': function (e) {
	    e.stopPropagation();
	    $('html, body').animate({
	        scrollTop: $("#plans").offset().top - 70
	    }, 420);
	},
    'click #signup1': function (e) {
        e.stopPropagation();
        $("html, body").animate({ scrollTop: 0 }, 420);
        if ($(window).width() < 768) {
           $('.navbar-header button').click();
           $('#login-dropdown-list .dropdown-toggle').click();
           $('#signup-link').click();
        }
        else {
           $("#login-dropdown-list").find('[data-toggle=dropdown]').dropdown('toggle');
           $('#signup-link').click();   
        }
    }
});

Accounts.ui.config({
    requestPermissions: {},
    extraSignupFields: [{
        fieldName: 'full-name',
        fieldLabel: 'Name',
        inputType: 'text',
        visible: true,
        validate: function(value, errorFunction) {
          if (value) {
          	return true;
          } else {
            errorFunction("Please enter your full name");
            return false;
          }
        }
    }, {
        fieldName: 'company-name',
        fieldLabel: 'Company Name',
        inputType: 'text',
        visible: true,
        validate: function(value, errorFunction) {
          if (!value) {
            errorFunction("Please enter your company name");
            return false;
          } else {
            return true;
          }
        }
    },{
        fieldName: 'plan',
        showFieldLabel: false,      // If true, fieldLabel will be shown before radio group
        fieldLabel: 'Plan',
        inputType: 'radio',
        radioLayout: 'vertical',    // It can be 'inline' or 'vertical'
        data: [{                    // Array of radio options, all properties are required
    		id: 1,                  // id suffix of the radio element
            label: 'Small Business',          // label for the radio element
            value: 'small'              // value of the radio element, this will be saved.
          }, {
            id: 2,
            label: 'Medium Business',
            value: 'medium'
            //checked: 'checked'
        },{
        	id: 3,
        	label: 'Enterprise',
        	value: 'enterprise',
        }],
        visible: true,
        validate: function(value, errorFunction) {
          if (!value) {
            errorFunction("Please select a plan type");
            return false;
          } else {
            return true;
          }
        }
    }, {
        fieldName: 'terms',
        fieldLabel: 'I accept the terms and conditions',
        inputType: 'checkbox',
        visible: true,
        saveToProfile: false,
        validate: function(value, errorFunction) {
            if (value) {
                return true;
            } else {
                errorFunction('You must accept the terms and conditions.');
                return false;
            }
        }
    }]
});


