var getSavedSearches = function () {
	var data = Router.current().data();
	return data || [];
}

var tableOptions = {
	stripeClasses: [ 'odd', 'even' ],
	columns: [{
		title: 'Search ID',
		data: 'searchId',
	}, {
		title: 'Description',
		data: 'description',
		width: '50%'
	}, {
		title: 'Data Headings',
		data: 'headings',
		render: function (data, type, row) {
			return Blaze.toHTMLWithData(Template.searchHeadings, data);
		}
	}, {
		title: 'Remove',
		render: function (data,type,row) {
			if (type === "display")
				return Blaze.toHTMLWithData(Template.deleteButton, row);
		}
	}],
};

Template.SearchList.helpers({
	tableOptions: tableOptions,
	savedSearches: function () {
		return getSavedSearches;
	},
});


Template.SearchList.events({
	'click .dataTable tr': function (e, tmpl) {
		var dt = $(".dataTable").DataTable();
		var rowData = dt.row(e.currentTarget).data();
		if (rowData.searchId) {
			Router.go("charts", {searchId: rowData.searchId});
		} else {
			swal("Please Try Again", "The data for that search is still loading", "info");
		}
	},
	'click button[data-search-id]': function (e, tmpl) {
		var searchId = $(e.currentTarget).data("searchId");
		if (_.isNumber(searchId) || (_.isString(searchId) && searchId.length)) {
			Netsuite.userSearches.remove(searchId);
		} else {
			sweetAlert("Error", "Couldn't find a Search ID for the item you want to remove", "error");
		}

		e.stopPropagation();
		e.stopImmediatePropagation();
		return false;
	}
});

Template.SearchList.usersOnline = function() {
  console.log(Meteor.users.find({ "status.online": true }).fetch());
};