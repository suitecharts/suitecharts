Template.Dialogue_AddSavedSearch.events({
	'submit form.do-add-saved-search': function (e) {
		e.preventDefault();
		var searchId = $('#saved-search-id').val();
		var description = $("#saved-search-description").val();

		$("#addSavedSearchModal").modal("hide");
		Netsuite.userSearches.add(searchId, description);
	}
});