Template.Dialogue_PromoteUser.events({
	'submit form.do-promote-user': function (e) {
		e.preventDefault();
		var email = $('#user-email').val();
		var role = $("#role").val();

		$("#promoteUserModal").modal("hide");

		Meteor.call("users/promoteToRole", email, role, function (err, res) {
			if (err) return swal("Error", (err.reason || err.message || err.details), "error");
			if (res) {
				console.log(res);
			}
			$('#user-email').val('');
			$("#role").val('');
		});
	}
});