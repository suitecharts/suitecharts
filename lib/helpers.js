_.mixin({
    findIn: function(o, s) {
        if(typeof o === 'undefined') return undefined;
        s = s.replace(/\[(\w+)\]/g, '.$1') // change [subObj] to .subObj notation
             .replace(/^\./, ''); // strip a leading dot
        var a = s.split('.');
        while (a.length) {
            var n = a.shift();
            if (typeof o[n] !== 'undefined') { // checking for the namespace 
                o = o[n];
            } else {
                return undefined;
            }
        }
        return o;
    }
});