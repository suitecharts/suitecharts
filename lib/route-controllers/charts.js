ChartsController = RouteController.extend({
	onBeforeAction: function () {
		// Refresh headings when entering chart
		// if(!this.ready()) return; // make sure we have some chart data
		var searchId = this.params.searchId;
		var self = this;

		Tracker.nonreactive(function () {
			Netsuite.userSearches.updateHeadingsFor(searchId, {
				silent: true,
				onComplete: self.next
			});
		});
	},
	data: function () {
		if(!this.ready()) return; // without this, caching is ignored

		var searchId = this.params.searchId;
		return searchId && Netsuite.search(searchId);
	},
	waitOn: function () {
		var searchId = this.params.searchId;
		if (searchId) {
			return Meteor.subscribe('netsuiteData', searchId);
		}
	},
});