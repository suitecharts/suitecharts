SearchListController = RouteController.extend({
	waitOn: function () {
		return Meteor.subscribe("Netsuite_SearchList");
	},
	data: function () {
		return Netsuite.Searches.find().fetch();
	}
});