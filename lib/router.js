Router.configure({
	waitOn: function () {
		return [
			Meteor.subscribe('netsuiteIsValid'),
			Meteor.subscribe("Netsuite_SearchList") // for the menu
		];
	},
	layoutTemplate: 'AppLayout',
	loadingTemplate: 'Loading'
}, {
	except: ['Login']
});


Router.onBeforeAction(function () {

	if (!this.ready()) return;

	var user = Meteor.user();

	if (!user) {
	
		this.render('Login');
	
	} else if (!user.services
					|| !user.services.netsuite
					|| !user.services.netsuite.isValid) {
		
		this.render('_NetsuiteLogin');
	
	} else {
		this.next();
	}
	
});


Router.map(function() {

	this.route('netsuite-login', {
		template: '_NetsuiteLogin'
	});

	this.route('searchList', {path: '/'});
	this.route('charts', {path: '/charts/:searchId'});

	this.route('dashboard', {path:'/dashboard/'});

});

