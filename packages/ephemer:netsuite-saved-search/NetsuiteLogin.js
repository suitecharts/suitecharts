Template._NetsuiteLogin.events({
	'submit #netsuite-login-form': function (e) {
		e.preventDefault();
		
		var $btn = $("#netsuite-login-form [type=submit]");
		$btn.attr("disabled", true);

		Netsuite.saveSettings({
			account: $('#account').val(),
			email: $('#email').val(),
			password: $('#password').val(),
			restlet: $('#restlet').val(),
		}, function (err, res){
			if (!err && res === true) {
				$btn.removeClass("btn-info")
					.addClass("btn-success")
					.text("Saved");
			}
			$btn.attr("disabled", false);
		});
	}
});


Template._NetsuiteLogin.helpers({
	settingsAlreadyValid: function () {
		var user = Meteor.user();
		return user
			&& user.services
			&& user.services.netsuite
			&& user.services.netsuite.isValid;
	}
});