// The cache's Time To Live can be set on the client:
// ---- Netsuite.cacheTTL.set(minutes)

// Any active search functions will react in real time

var defaultTTL = 30; // minutes
Netsuite.cacheTTL = new ReactiveVar(defaultTTL);


Netsuite.search = function (searchId, options) {

	if (!searchId) throw new Meteor.Error(499, "You must provide a searchId");
	searchId += ""; // convert to string if it exists

	options = options || {};

	var result = Netsuite.Searches.findOne({
		searchId: searchId
		// XXX: use options.filter
	});

	// Cache is considered expired if fetchedAt more than (default) 30mins ago
	// Note, there can be client/server time discrepancies, but normally this
	// is a matter of seconds rather than minutes (and is as such no problem).
	var now = moment();
	var ttl = Netsuite.cacheTTL.get();
	var cacheExpired = !result || !result.fetchedAt;

	// Update the document if we're unhappy with current cached state
	if (cacheExpired || options.forceUpdate) {
		console.log("Updating cache for", searchId);
		Meteor.call('netsuite/updateCache', searchId, options, function (err) {
			if (err) {
				if (err.reason === "INVALID_LOGIN_CREDENTIALS") {
					swal("Error", "Your Netsuite credentials appear invalid. Please update them on the Netsuite Login screen.", "error");
					Meteor.call('netsuite/invalidateCredentials');
				} else if (err.error !== "You must be logged in") {
					swal("Error", err.details, "error");
				}
				console.warn(err);
			} else {
				console.log("Updated cache for searchId", searchId);
			}
		});
	}

	// search should be run in a reactive context, e.g. in a route's data
	// function or a template helper, meaning if result is undefined the first
	// time, it will be reactively updated with the data grabbed above.

	// The benefit of this is that any available cached data will first be shown,
	// if it exists, and then updated.

	return result;

}

Netsuite.saveSettings = function (settings, cb) {
	Meteor.call('netsuite/saveSettings', settings, cb);
};


// Check whether to show NetsuiteLogin page or not
Meteor.subscribe('netsuiteIsValid');