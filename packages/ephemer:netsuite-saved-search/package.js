// Fetch and cache a Netsuite saved search, for meteor.js
// Copyright Geordie Jay 2014

// Inspired by the npm module netsuite-saved-search, specifically this file:
// https://github.com/ahinni/netsuite-saved-search/blob/master/lib/netsuite-saved-search.js

Package.describe({
  summary: "Fetch and cache Netsuite saved searches",
  version: "0.1.1",
  //git: ""
});

Package.onUse(function(api) {
  
  api.versionsFrom('METEOR@0.9.3.1');
  
  api.use(['oauth-encryption', 'mongo', 'reactive-var', 'templating']);
  api.addFiles('shared.js', ['client', 'server']);
  api.addFiles(['client.js', 'NetsuiteLogin.html', 'NetsuiteLogin.js'], 'client');
  api.addFiles(['publication.js', 'server.js'], 'server');
  
  api.export('Netsuite');

});

/*Package.onTest(function(api) {
  api.use('tinytest');
  api.use('ephemer:netsuite-saved-search');
  api.addFiles('ephemer:netsuite-saved-search-tests.js');
});
*/