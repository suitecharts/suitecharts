Meteor.publish('netsuiteData', function (searchId) {
	
	try {
		var userId = this.userId;
		var netsuite = Netsuite.getSettingsFor(userId);
	} catch (e) {
		return this.ready();
	}

	return Netsuite.Searches.find({
		// the combination of these query parameters makes
		// it difficult to access someone else's data
		userId: userId,
		searchId: searchId,
		restlet: netsuite.restlet,
		account: netsuite.account,
	}, {
		fields: {
			account: 0,
			restlet: 0,
			userId: 0
		}
	});
	
});


Meteor.publish('netsuiteIsValid', function () {
	if(!this.userId) return this.ready();
	return Meteor.users.find(this.userId, {
		fields: {'services.netsuite.isValid': 1}
	})
});