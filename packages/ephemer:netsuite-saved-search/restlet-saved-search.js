// This file must be uploaded to Netsuite and deployed as a RESTlet

function executeSavedSearch(options) {
  if ( !options.searchId ) {
    return { error: 'Must provide the searchId of the saved search', options: options };
  }

  var search = nlapiLoadSearch(null, options.searchId);
  
  var resultset = search.runSearch();

  var column_list = resultset.getColumns();
  var col_len = column_list.length;

  var results = [];  
  var line = [];
  var columnNames = [];

  // Get the labels
  for (var i = 0; i < col_len; i++) {
    var column = column_list[i];
    var colLabel = column.getLabel();
    columnNames.push (colLabel);
  };
  
  var rowArray = Array();
  var SLICE_LIMIT = 1000;
  var index = 0;
  
  do {
    var subset = resultset.getResults(index, index+1000);
    if ( !subset ) break;

    subset.forEach( function (row) {
      var JSONrow = JSON.stringify(row); // Parse to JSON
      rowArray.push(JSON.parse(JSONrow)); // Parse back to Array (only way to access column data)
      index++;
    });

  } while (subset.length === SLICE_LIMIT);
  
  // For each item of the row array we will create a new object to replace the original
  rowArray.forEach(function(entry) {
    var newRow = {"id":entry['id'],"recordtype":entry['recordtype']}; // Initialize new row
    var k = 0;
    var rowColumns = {};

    for(var index in entry['columns']) { 
      rowColumns[columnNames[k]] = entry['columns'][index]; // For each column of the row : [label] = value
      k++;
    }
  
    newRow['columns'] = rowColumns; // Add columns to new row
    results.push(newRow);

  });
  
  nlapiLogExecution('DEBUG','row', JSON.stringify(results));
  return results;
}

