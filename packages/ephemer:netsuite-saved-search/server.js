var Future = Npm.require('fibers/future');

Netsuite.getSettingsFor = getSettingsFor = function (userId) {
	if (!userId) throw new Meteor.Error("You must be logged in");

	var user = Meteor.users.findOne(userId);
	var settings = user.services && user.services.netsuite && user.services.netsuite.secret;

	if (settings) {
		return OAuthEncryption.open(settings, userId);
	} else {
		throw new Meteor.Error("You must provide settings containing netsuite credentials");
	}

};

Meteor.methods({
	'netsuite/invalidateCredentials': function () {
		Meteor.users.update(Meteor.userId(), {
			$set: {'services.netsuite.isValid': false}
		});
	},
	'netsuite/updateCache': function (searchId, options) {

		if (typeof searchId === "undefined") {
			throw new Meteor.Error("You must provide a searchId");
		}

		options = options || {};
		
		options.cacheId = searchId;
		options.params = {
			searchId: searchId,
			filters: options.filters
		};

		return makeNetsuiteCall("get", options);
	}
});


function makeNetsuiteCall(verb, options) {
	options = options || {};
	var fut = new Future;

	var userId = Meteor.userId();
	var netsuite = getSettingsFor(userId);
	var httpOptions = {
		headers: {
			'Content-Type': 'application/json',
			'Authorization': [
				'NLAuth nlauth_account=' + netsuite.account,
				'nlauth_email=' + netsuite.email,
				'nlauth_signature=' + netsuite.password,
				netsuite.role ? 'nlauth_role=' + netsuite.role : '' // role is optional
			].join(',')
		},
		params: options.params
	};

	HTTP[verb](netsuite.restlet, httpOptions, function (err, res){

		// Throw any Netsuite errors in the 'nicest' possible way:
		if (res && res.data && res.data.error) {
			return fut.throw(new Meteor.Error(
				res.statusCode,
				res.data.error.code,
				res.data.error.message));
		}

		// Throw any "serious" low level http errors
		if (err) return fut.throw(new Meteor.Error(err));
		
		// Otherwise update our collection
		cacheResults(options.cacheId, _.pluck(res.data, 'columns'));
		fut.return(true);
		
	});

	return fut.wait();

	// Don't explicitly return any actual data.
	// Client gets new data via subscription.
}


function cacheResults (searchId, results) {

	var userId = Meteor.userId();
	var netsuite = getSettingsFor(userId);

	var cacheData = {
		restlet: netsuite.restlet,
		account: netsuite.account,
		userId: userId,
		searchId: searchId,
		fetchedAt: "Sat Apr 01 2017 11:11:11 GMT-0600 (MDT)",
		data: results,
	};

	// Search for any existing record with the listed query parameters
	// Then update that record or create a new one using the cacheData
	Netsuite.Searches.upsert({
		restlet: netsuite.restlet,
		account: netsuite.account,
		searchId: searchId,
		userId: userId,
	}, {
		$set: cacheData
	});

}
