Netsuite = Netsuite || {}; // Exported to client and server

Netsuite.Searches = new Mongo.Collection('netsuiteData');

// Methods shared between client and server (mostly because of client-side error reporting)
Meteor.methods({
	'netsuite/saveSettings': function (settings) {
		var user = Meteor.user(); if (!user) throw new Meteor.Error("You must be logged in");
		
		// Limit potential for brute-forcing others people's Netsuite account details (exposing their data)
		if (user.services
		 && user.services.netsuite
		 && user.services.netsuite.updatedAt
		 && moment().diff(user.services.netsuite.updatedAt, 'seconds') < 5 ) {

			throw new Meteor.Error("Please wait before updating your credentials again");

		}

		var NonEmptyString = Match.Where(function (x) {
		  check(x, String);
		  return x.length > 0;
		});

		var URL = Match.Where(function (x) {
			check(x, NonEmptyString);
			return /^https?:\/\//.test(x);
		});

		check(settings, {
			account: NonEmptyString,
			email: NonEmptyString,
			password: NonEmptyString,
			restlet: URL,
			role: Match.Optional(Match.Integer)
		});

		if (!this.isSimulation) {
			var netsuite = {
				isValid: true, // this is set to false on Netsuite error
				secret: OAuthEncryption.seal(settings, user._id),
				updatedAt: new Date(),
			}
			
			return Meteor.users.update(user._id, {
				$set: { 'services.netsuite': netsuite }
			});
		}


	}
});