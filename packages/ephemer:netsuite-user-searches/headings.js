// switch between sweetAlert and console.log depending on the "silent" option

Netsuite.userSearches.updateHeadingsFor = function (searchId, options) {

	options = options || {};

	var swal = !options.silent ? sweetAlert : function () {
		// don't popup message dialogues in silent mode, log to console instead
		Function.prototype.apply.call(console.log, console, arguments);
	};

	var dataSubscription = Meteor.subscribe('netsuiteData', searchId);
	var retries = 0;

	Tracker.autorun(function(comp) {

		var results = Netsuite.search(searchId);
		
		// keep looking for data until we get it:
		if (retries < 3 && !results || !results.data || !dataSubscription.ready()) return retries++;

		dataSubscription.stop();
		comp.stop();

		// We still don't have data after 3 retries
		if (!results.data) {
			return swal("Error",
				"Sorry! There was an error building your chart! Please modify your Saved Search.",
				"error");
		}

		// Find the headings (key names), recursively, for the given piece of data
		var firstEntry = _.first(results.data);
		var lastEntry = _.last(results.data);	
		var firstHeadings = listHeadingsFor(firstEntry);
		var lastHeadings = listHeadingsFor(lastEntry);

		var headingsToSave = [];
		
		// Compare the headings for the first and last objects
		if (!_.isEqual(firstHeadings, lastHeadings)) {
			swal("Warning! There is a mismatch in the headers of your data!", "warning");
			headingsToSave = (firstHeadings.length > lastHeadings.length) ? firstHeadings : lastHeadings;
		} else {
			headingsToSave = firstHeadings;
		}

		//console.log("Adding headings to:", results._id);

		// Save to database
		Netsuite.Searches.update(results._id, {
			$set: { headings: headingsToSave }
		}, function (err) {
			if (err) swal("Sorry! We're having an issue showing your chart. Please try again.", (err.message || err.reason), "error");
			/*
			else swal("Updated headings", _.map(headingsToSave, function (value) {
				return value.description;
			}).join("\n"), "success");
			*/
		});

		if (_.isFunction(options.onComplete)) options.onComplete();

		//console.log(headingsToSave);

	});
}


function listHeadingsFor(data) {
	var headings = [];
	if (_.isObject(data)) {
		concatenateHeadings(data);
	}
	return headings;

	function concatenateHeadings (obj, prefix) {
		// Set a prefix for the heading strings
		// e.g. with prefix === ["employee"] -> "employee.name"
	  prefix = prefix || [];
	  
	  _.pairs(obj).map(function (pair, index, list){
	    // pair: ["durationdecimal", 47]
	    // pair[0] = key;
	    // pair[1] = value;

	    if (_.isObject(pair[1])) {
	    	// Iterate for subobjects
	      return concatenateHeadings(pair[1], prefix.concat(pair[0]));
	    } else {
	    	// Return a prefix pair as an object like so:
	    	// { description: "employee.name", type: "string" }
	      pair[0] = prefix.concat(pair[0]).join('.');
	      pair[1] = typeof pair[1];
	      pair = _.object([["description", pair[0]], ["type", pair[1]]])
	      headings.unshift(pair);
	    }
	  });
	}
}
