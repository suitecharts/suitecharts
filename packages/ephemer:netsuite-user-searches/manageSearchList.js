Netsuite.userSearches = {};
Netsuite.userSearches.add = function (searchId, description) {
	var userId = Meteor.userId(); if (!userId) return;
	
	if (!searchId) {
		return swal("Oops...", "You must enter a Search ID!", "error");
	} else if (!description) {
		return swal("Oops...", "You must enter a description for your search!", "error");
	}

	searchId += ""; // ensure it's always a string

	// Note, this will currently overwrite any existing search with that ID
	// XXX: needs some UX/UI for confirm overwrite / cancel.

	Netsuite.Searches.upsert({ searchId: searchId }, {
		$set: { description: description }
	}, function (err, res){
		if (!err) {
			
			if (res.insertedId) {
				swal("Success", "Added " + searchId + " to your list of saved searches", "success");
			} else {
				swal("Success", "Updated description for existing search: '" + searchId + "'", "success");
			}

			Netsuite.userSearches.updateHeadingsFor(searchId);

		} else {
			swal("Error", "Couldn't add your search" + (err.message || err.reason), "error");
		}
	});
}



Netsuite.userSearches.remove = function (searchId) {

	if (!searchId) {
		return swal("Oops...", "You must provide a Search ID to remove!", "error");
	}

	var existingSearch = Netsuite.Searches.findOne({ searchId: searchId });

	if (!existingSearch && _.isNumber(searchId)) {
		return Netsuite.userSearches.remove(searchId + ""); // retry as a string
	} else if(!existingSearch) {
		// if we still didn't find one
		return swal("Error", "That search doesn't appear to exist!", "error");
	}

	Netsuite.Searches.remove(existingSearch._id, function (err) {
		if (err) console.warn("Error deleting search", err);
	});

}

// Manual override of upsert
Netsuite.Searches.upsert = function (query, modifier, callback) {
	Meteor.call("netsuite/upsertSearch", query, modifier, callback);
}