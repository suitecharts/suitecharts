Meteor.methods({
	"netsuite/upsertSearch": function (query, modifier) {
		
		var userId = this.userId;
		var netsuiteAccount = Netsuite.getSettingsFor(userId);
		if (!userId || !netsuiteAccount) throw new Meteor.Error(400, "Permission denied");

		query.userId = userId;
		query.restlet = netsuiteAccount.restlet;
		query.account = netsuiteAccount.account;
		
		if (typeof query.searchId === "undefined") {
			throw new Meteor.Error(499, "Missing data");
		}

		return Netsuite.Searches.upsert(query, modifier);
		
	}
})