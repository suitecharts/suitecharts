Package.describe({
  name: 'ephemer:netsuite-user-searches',
  summary: 'Add features to customise Netsuite saved searches (especially editable auto-generated data headings)',
  version: '1.0.0',
  //git: ' /* Fill me in! */ '
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use(['ephemer:netsuite-saved-search']);
  api.addFiles('manageSearchList.js', 'client');
  api.addFiles('headings.js', 'client');
  api.addFiles('publish-search-list.js', 'server');
  api.addFiles('methods.js', 'server');
});

