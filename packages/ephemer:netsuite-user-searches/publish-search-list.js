Meteor.publish("Netsuite_SearchList", function () {

	try {
		var userId = this.userId;
		var netsuite = Netsuite.getSettingsFor(userId);
	} catch (e) {
		return this.ready();
	}

	return Netsuite.Searches.find({
		// the combination of these query parameters makes
		// it difficult to access someone else's data
		userId: userId,
		restlet: netsuite.restlet,
		account: netsuite.account,
	}, {
		fields: {
			account: 0,
			restlet: 0,
			userId: 0,
			data: 0, // use "netsuiteData" subscription to access search data
		}
	});
});


var allowedFields = ["description", "headings", "chartType"];

Netsuite.Searches.allow({
	update: function (userId, doc, fields, modifier) {
		if (doc.userId === userId && _.difference(fields, allowedFields).length === 0) {
			return true;
		} else {
			throw new Meteor.Error(400, "Permission denied");
		}
	},
	remove: function (userId, doc) {
		if (doc.userId === userId) {
			return true; // users can remove their own searches
		} else {
			console.log(userId, "illegally tried to remove", doc);
		}
	},
	fetch: ['searchId', 'userId'],
});