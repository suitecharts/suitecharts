Meteor.methods({
  
  /**
   * update a user's permissions
   *
   * @param {String} email: User to update's email address
   * @param {Array/String} roles: User's new permission(s)
   */

  "users/promoteToRole": function (email, roles) {
    var loggedInUser = Meteor.user();

    if (!loggedInUser ||
        !Roles.userIsInRole(loggedInUser, ['admin'])) {
      throw new Meteor.Error(403, "Access denied");
    }

    var usersWithGivenEmail = Meteor.users.find({
      "emails.address": email
    }, {
      fields: { _id: 1 }
    }).fetch();

    if (usersWithGivenEmail.length === 0){
      throw new Meteor.Error("Couldn't find a user with that email");
    } else if (usersWithGivenEmail.length > 1){
      throw new Meteor.Error("More than one user found with that email");
    } else if (usersWithGivenEmail[0]._id === loggedInUser._id) {
      throw new Meteor.Error("You can't change your own user's role");
    } else {
      
      var idOfPromotedUser = usersWithGivenEmail[0]._id;
      if (_.isString(roles)) roles = [roles];

      return Roles.setUserRoles(idOfPromotedUser, roles);

    }

  }
})